import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;

@ScriptMeta(developer = "Koldolmen", desc = "Mines adamant ores in mining guild", name = "AddyMiner")
public class AddyMiner extends TaskScript
{
    public static final Area BANK_AREA = Area.rectangular(new Position(3009, 3358, 0), new Position(3018, 3355, 0));
    public static final Area STONES_AREA = Area.rectangular(new Position(3033, 9732, 0), new Position(3042, 9738, 0));
    public static final Area LADDERS_UPSTAIRS = Area.rectangular(new Position(3014, 3342, 0), new Position(3024, 3336, 0));
    public static final Area LADDERS_DOWNSTAIRS = Area.rectangular(new Position(3017, 9741, 0), new Position(3026, 9736, 0));
    public static final Position STONE1 = new Position(3034, 9732, 0);
    public static final Position STONE2 = new Position(3040, 9736, 0);
    public static final int PICKAXE_ID = 1275;
    public static int totalStones = 0;


    private static final Task[] TASKS = {new Banking(), new Traverse(), new Mine(), new WorldHop()};

    @Override
    public void onStart()
    {
        submit(TASKS);
    }

    @Override
    public void onStop(){
        Log.info(totalStones);
    }
}
