import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class Mine extends Task
{
    @Override
    public boolean validate()
    {
        SceneObject stone1 = SceneObjects.getFirstAt(AddyMiner.STONE1);
        SceneObject stone2 = SceneObjects.getFirstAt(AddyMiner.STONE2);
        short[] colors1 = null;
        short[] colors2 = null;
        if (stone1 != null && stone2 != null)
        {
            colors1 = stone1.getDefinition().getNewColors();
            colors2 = stone2.getDefinition().getNewColors();
        }


        return !Inventory.isFull() && AddyMiner.STONES_AREA.contains(Players.getLocal()) && (colors1 != null || colors2 != null) && !Players.getLocal().isAnimating();
    }

    @Override
    public int execute()
    {
        SceneObject stone1 = SceneObjects.getFirstAt(AddyMiner.STONE1);
        SceneObject stone2 = SceneObjects.getFirstAt(AddyMiner.STONE2);
        short[] colors1 = stone1.getDefinition().getNewColors();
        short[] colors2 = stone2.getDefinition().getNewColors();

        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > 80)
        {
            Movement.toggleRun(true);
        }

        if (stone1.distance() < stone2.distance())
        {
            if (stone1 != null && colors1 != null)
            {
                stone1.interact("Mine");
                Time.sleepUntil(() -> stone1.getDefinition().getNewColors() == null, 2000);
            }
            else if (stone2 != null && colors2 != null)
            {
                stone2.interact("Mine");
                Time.sleepUntil(() -> stone2.getDefinition().getNewColors() == null, 2000);
            }
        }
        else
        {
            if (stone2 != null && colors2 != null)
            {
                stone2.interact("Mine");
                Time.sleepUntil(() -> stone2.getDefinition().getNewColors() == null, 2000);
            }
            else if (stone1 != null && colors1 != null)
            {
                stone1.interact("Mine");
                Time.sleepUntil(() -> stone1.getDefinition().getNewColors() == null, 2000);
            }
        }


        return 1000;
    }
}
