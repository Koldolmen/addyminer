import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.Scene;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class Traverse extends Task
{
    private boolean isUpstairs = false;

    @Override
    public boolean validate()
    {
        return traverseToBank() || traverseToStones();
    }

    @Override
    public int execute()
    {
        //Movement.walkTo(traverseToBank() ? AddyMiner.BANK_AREA.getCenter() : AddyMiner.STONES_AREA.getCenter());
        Player player = Players.getLocal();

        if(!Movement.isRunEnabled() && Movement.getRunEnergy() > 80){
            Movement.toggleRun(true);
        }

        if (traverseToBank())
        {
            if (!isUpstairs && !AddyMiner.LADDERS_DOWNSTAIRS.contains(player))
            {
                Movement.walkTo(AddyMiner.LADDERS_DOWNSTAIRS.getCenter());
            }
            else if (!isUpstairs && AddyMiner.LADDERS_DOWNSTAIRS.contains(player))
            {
                SceneObject ladder = SceneObjects.getNearest("Ladder");
                if (ladder != null)
                {
                    ladder.interact("Climb-up");
                    Time.sleepUntil(() -> AddyMiner.LADDERS_UPSTAIRS.contains(player), 1000);
                    isUpstairs = true;
                }
            }
            else if (isUpstairs && !AddyMiner.BANK_AREA.contains(player))
            {
                Bank.open(BankLocation.FALADOR_EAST);
            }

        }
        else
        {
            if (!AddyMiner.LADDERS_UPSTAIRS.contains(player) && isUpstairs)
            {
                Movement.walkTo(AddyMiner.LADDERS_UPSTAIRS.getCenter());
            }
            else if (AddyMiner.LADDERS_UPSTAIRS.contains(player))
            {
                SceneObject ladder = SceneObjects.getNearest("Ladder");
                if (ladder != null)
                {
                    ladder.interact("Climb-down");
                    Time.sleepUntil(() -> AddyMiner.LADDERS_UPSTAIRS.contains(player), 1000);
                    isUpstairs = false;
                }
            }
            else if (AddyMiner.LADDERS_DOWNSTAIRS.contains(player) && !AddyMiner.STONES_AREA.contains(player) && !isUpstairs)
            {
                Movement.walkTo(AddyMiner.STONES_AREA.getCenter());
            }
        }

        return 3000;
    }

    private boolean traverseToBank()
    {
        return Inventory.isFull() && !AddyMiner.BANK_AREA.contains(Players.getLocal());
    }

    private boolean traverseToStones()
    {
        return !Inventory.isFull() && !AddyMiner.STONES_AREA.contains(Players.getLocal()) && !Players.getLocal().isAnimating();
    }
}
