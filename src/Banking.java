import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

public class Banking extends Task
{
    @Override
    public boolean validate()
    {
        return AddyMiner.BANK_AREA.contains(Players.getLocal()) && Inventory.isFull();
    }

    @Override
    public int execute()
    {
        if (Bank.isOpen())
        {
            Bank.depositAllExcept(AddyMiner.PICKAXE_ID);
            Time.sleepUntil(() -> Inventory.containsOnly(AddyMiner.PICKAXE_ID), 1000);
            AddyMiner.totalStones += 27;
        }
        else{
            Bank.open(BankLocation.FALADOR_EAST);
        }
        return 300;
    }
}
