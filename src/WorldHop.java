import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.component.WorldHopper;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.providers.RSWorld;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class WorldHop extends Task
{
    @Override
    public boolean validate()
    {
        SceneObject stone1 = SceneObjects.getFirstAt(AddyMiner.STONE1);
        SceneObject stone2 = SceneObjects.getFirstAt(AddyMiner.STONE2);
        short[] colors1 = null;
        short[] colors2 = null;

        if (stone1 != null && stone2 != null)
        {
            colors1 = stone1.getDefinition().getNewColors();
            colors2 = stone2.getDefinition().getNewColors();
        }

        return AddyMiner.STONES_AREA.contains(Players.getLocal()) && colors1 == null && colors2 == null;
    }

    @Override
    public int execute()
    {
        Log.info("Byter värld");

        WorldHopper.randomHopInF2p();
        return 1500;
    }
}
